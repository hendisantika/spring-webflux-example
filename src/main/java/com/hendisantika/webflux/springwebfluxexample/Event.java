package com.hendisantika.webflux.springwebfluxexample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-webflux-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-13
 * Time: 09:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {
    private long id;
    private Date date;
}